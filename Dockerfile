#
# --- Builder ---
FROM golang:1.12-alpine as builder

WORKDIR /app

# Required by `go mod download`
RUN apk add git
ENV GO111MODULE=on

COPY go.mod .
COPY go.sum .
RUN go mod vendor

COPY . .
RUN go build -o main ./cmd/app

#
# --- Runner ---
FROM alpine

WORKDIR /app

COPY --from=builder /app/main .

CMD ["./main"]
