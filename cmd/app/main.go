package main

import (
	"net/http"

	log "github.com/sirupsen/logrus"
	config "gitlab.com/esayemm-boilerplates/go-hello/config"
)

func main() {
	conf := config.New()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello"))
	})

	log.Info("Server listening on port " + conf.Port)
	log.Fatal(http.ListenAndServe(":"+conf.Port, nil))
}
