package config

import "os"

// Config contains app configurations
type Config struct {
	Port string
}

func getEnvOr(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// New returns map[string]string of .env vars
func New() *Config {
	return &Config{
		Port: getEnvOr("PORT", "8020"),
	}
}
