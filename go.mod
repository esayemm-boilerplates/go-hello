module gitlab.com/esayemm-boilerplates/go-hello

go 1.12

require (
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.2.2
)
